==========================================================================
1. Servidor
2. Site
3. Aplicativo
4. Documentação de apoio
5. Contas
==========================================================================

=== 1. Servidor ===
Requisitos:
	Java 		: https://www.java.com/pt_BR/
	Eclipse 	: https://eclipse.org/downloads/
	PostgreSQL	: https://www.postgresql.org/
	Ruby 		: http://rubyinstaller.org/downloads/ #Deve ser a versão 1.9.3
	Ant			: http://ant.apache.org/bindownload.cgi
	Maven		: https://maven.apache.org/install.html
	WildFly		: http://wildfly.org/downloads/ #Deve ser a versão wildfly-10.0.0.Final	

Adicionar as variáveis de ambiente
	OPENSHIFT_POSTGRESQL_DB_HOST	 : 127.0.0.1
	OPENSHIFT_POSTGRESQL_DB_PORT	 : FRibzMqadVSA
	OPENSHIFT_APP_NAME				 : 5433
	OPENSHIFT_POSTGRESQL_DB_USERNAME : adminptuz3ul
	OPENSHIFT_DATA_DIR				 : <SEU DIRETORIO LOCAL DE IMAGENS> Ex:C:\tcc\servidor\imagens\

Ir até a pasta jbosswildfly e executar o comando:
	mvn package
	
Abrir o projeto no eclipse e dar um Refresh e um Clean no projeto.

Configurar o servidor WildFly no eclipse
	http://www.mauda.com.br/?p=606

Para levantar o servidor deve-se mapear as portas do banco de dados no openshift (https://developers.openshift.com/managing-your-applications/port-forwarding.html)
	rhc portforward jbosswildfly
	
O servidor possui dois endpoints de testes para popular e deletar a base
	Delete
		https://jbosswildfly-furbmobile2016.rhcloud.com/api/gincana/delete
	Popular
		https://jbosswildfly-furbmobile2016.rhcloud.com/api/gincana/popular
Atenção: Caso algum dia o aplicativo entre em produção esses dois endpoins devem ser retirados

=== 2. Site ===
Requisitos
	NodeJS: https://nodejs.org/en/download/ #Versão LTS

Ir até a pasta gincanaweb e executar o comando:
	npm install
	bower install
	
No arquivo "configValue.js" é definido o caminho do back-end (no openshift ou localhost).

Para iniciar o site:
	grunt serve
	
Para liberar no ambiente openshift deve-se copiar os arquivos novos/alterados para o diretório do servidor (..\gincanaserver\jbosswildfly\src\main\webapp)

=== 3. Aplicativo ===
Requisitos
	NodeJS	: https://nodejs.org/en/download/ #Versão LTS
	Cordoca	: npm install -g cordova
	Ionic	: npm install -g ionic

Ir até a pasta FurbMobile e executar o comando:
	npm install
	ionic platform add android (ou ios)
	ionic plugin add cordova-plugin-camera
	ionic plugin add cordova-plugin-compat
	ionic plugin add cordova-plugin-console
	ionic plugin add cordova-plugin-device
	ionic plugin add cordova-plugin-geolocation
	ionic plugin add cordova-plugin-inappbrowser
	ionic plugin add cordova-plugin-splashscreen
	ionic plugin add cordova-plugin-statusbar
	ionic plugin add cordova-plugin-whitelist
	ionic plugin add ionic-plugin-keyboard
	ionic plugin add phonegap-plugin-push -variable SENDER_ID=<GCM KEY>  (Para a conta furbmobile2016@gmail está configurado o projeto furbmobile com o SENDER_ID 542982183896. Mais detalhes em http://docs.ionic.io/docs/push-full-setup)

No arquivo "configValue.js" é definido o caminho do back-end (no openshift ou localhost).

Para rodar o aplicativo:
	ionic build
	Android:
		ionic run android
	iOS
		Abrir no x-code o projeto que está na pasta (...\FurbMobile\platforms\ios)

== 5. Integrações ===

Facebook
Deve-se criar um projeto no Facebook
	https://ccoenraets.github.io/ionic-tutorial/ionic-facebook-integration.html
	https://developers.facebook.com/docs/facebook-login

As APIs do Google Maps e do YouTube já estão ativadas para a conta furbmobile2016@gmail
	
=== 5. Contas ===
---- GMAIL ----
email: furbmobile2016@gmail.com
senha: mobilefurb2016
---- ///// ----

---- OPENSHIFT ----
https://openshift.redhat.com/app/console/applications
email: furbmobile2016@gmail.com
senha: mobilefurb2016

Projeto: jbosswildfly
GIT do servidor: ssh://56ec1d277628e1b45f000216@jbosswildfly-furbmobile2016.rhcloud.com/~/git/jbosswildfly.git/

Site: https://jbosswildfly-furbmobile2016.rhcloud.com/ (aguardar alguns minutos na primeira tentativa pois o servidor pode estar parado)
---- ///////// ----

---- IONIC PLATFORM ----
http://ionic.io/
email: furbmobile@furb.br
senha: mobilefurb2016

Projeto: FurbMobile (64a98ff2)
---- ///////// ----

---- Trello ----
https://trello.com/
email: furbmobile2016@gmail.com
senha: mobilefurb2016

Chave API: https://trello.com/app-key
---- ///// ----