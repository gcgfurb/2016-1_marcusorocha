angular.module("furbMobile")
	.controller("gruposCtrl", ["$scope", "$rootScope", "$state", "grupoAPI", "$ionicLoading", "$ionicPopup", function ($scope, $rootScope, $state, grupoAPI, $ionicLoading, $ionicPopup) {
		
		var vm = this;

        $scope.getGrupos = function () {

			$ionicLoading.show({
				template: '<p>Carregando grupos...</p><ion-spinner></ion-spinner>'
			});
			grupoAPI
				.getGrupos($scope.gincana)
				.then(
				function onSuccess(params) {
					$scope.grupos = params.data;
					vm.selecionado = {id: 0};
					$ionicLoading.hide();
				})
				.catch(function onError(params) {
					$ionicLoading.hide();
					$ionicPopup.alert({
						title: "Não foi possível executar a operação.",
						template: "Ocorreu um erro ao tentar executar esta operação"
					});
				})
				.finally(function () {
					$scope.$broadcast('scroll.refreshComplete');
				});

		}

		$scope.entrarGrupo = function () {
			$ionicLoading.show({
				template: '<p>Entrando no grupo...</p><ion-spinner></ion-spinner>'
			});
			grupoAPI
				.vincular($rootScope.usuario.id, vm.selecionado.id)
				.then(function onSuccess(params) {
					$ionicLoading.hide();
					$scope.fecharModal();
				})
                .catch(function onError(params) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: "Não foi possível executar a operação.",
                        template: "Ocorreu um erro ao tentar executar esta operação"
                    });
                });
		};

		$scope.getGrupos();

	}]);