angular.module("furbMobile")
    .controller("delegarTarefaCtrl", ["$scope", "usuarioAPI", "tarefaAPI", "$ionicLoading", "$ionicPopup", function ($scope, usuarioAPI, tarefaAPI, $ionicLoading, $ionicPopup) {

        $scope.getUsuarios = function () {
            $ionicLoading.show({
                template: '<p>Carregando usuários...</p><ion-spinner></ion-spinner>'
            });
            usuarioAPI
                .getUsuariosDisponiveis($scope.grupo)
                .then(function onSuccess(params) {
                    $scope.usuarios = params.data;
                    $scope.usuarios.selecionado = 0;
                    $ionicLoading.hide();
                })
                .catch(function onError(params) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: "Não foi possível executar a operação.",
                        template: "Ocorreu um erro ao tentar executar esta operação"
                    });
                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
        }

        $scope.atribuirTarefa = function () {
            if ($scope.usuarios.selecionado != 0) {
                $ionicLoading.show({
                    template: '<p>Associando com usuário...</p><ion-spinner></ion-spinner>'
                });
                tarefaAPI
                    .atribuirTarefa($scope.usuarios.selecionado, $scope.tarefaGrupo)
                    .then(function onSuccess(params) {
                        $ionicLoading.hide();
                        $scope.fecharModal();
                    })
                    .catch(function onError(params) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: "Não foi possível executar a operação.",
                            template: "Ocorreu um erro ao tentar executar esta operação"
                        });
                    });
            }
        };

        $scope.getUsuarios();
    }]);