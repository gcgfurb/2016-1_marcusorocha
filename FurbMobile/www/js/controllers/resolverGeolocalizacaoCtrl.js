angular
    .module("furbMobile")
    .controller("resolverGeolocalizacaoCtrl", ["$scope", "tarefaAPI", "NgMap", "$ionicLoading", "$ionicPopup", function ($scope, tarefaAPI, NgMap, $ionicLoading, $ionicPopup) {

        $scope.obj = { renderizarMapa: false };
        $scope.posicaoAtual = { latitude: 0, longitude: 0 };
        $scope.mapa = undefined;

        $scope.resolverGeolocalizacao = function () {
            if (!$scope.mapa.shapes.circle.getBounds().contains($scope.mapa.markers[0].position)) {
                $ionicPopup.alert({
                    title: "Atenção",
                    template: "Você não está dentro da área da tarefa!"
                });
                return;
            }

            $ionicLoading.show({
                template: '<p>Confirmando resposta...</p><ion-spinner></ion-spinner>'
            });
            tarefaAPI
                .resolverGeolocalizacao($scope.tarefaGrupo.id, $scope.mapa.markers[0].position)
                .then(function onSuccess(params) {
                    $ionicLoading.hide();
                    $scope.fecharModal();
                })
                .catch(function onError(params) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: "Não foi possível executar a operação.",
                        template: "Ocorreu um erro ao tentar executar esta operação"
                    });
                });
        };

        $scope.centralizarObjetivo = function () {
            if ($scope.mapa)
                $scope.mapa.setCenter(new google.maps.LatLng($scope.tarefaGrupo.tarefa.latitude, $scope.tarefaGrupo.tarefa.longitude));
            else
                $scope.mostrarMapa();
        };

        $scope.centralizarPosicao = function () {
            if ($scope.mapa && ($scope.posicaoAtual.latitude != 0 && $scope.posicaoAtual.longitude != 0))
                $scope.mapa.setCenter(new google.maps.LatLng($scope.posicaoAtual.latitude, $scope.posicaoAtual.longitude));
            else
                $scope.mostrarMapa();
        }

        $scope.mostrarMapa = function () {
            NgMap.getMap("mapa").then(function (map) {
                $scope.mapa = map;
                navigator.geolocation.getCurrentPosition(onMostrarMapaSuccess, onMapErrorFirstTime, { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true });
            });
            return true;
        };

        var onMostrarMapaSuccess = function (position) {
            $scope.posicaoAtual.latitude = position.coords.latitude;
            $scope.posicaoAtual.longitude = position.coords.longitude;

            $scope.mapa.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            $scope.mapa.markers[0].setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            $scope.mapa.setZoom(13);

            return navigator.geolocation.watchPosition
                (onMapWatchSuccess, onMapError, { enableHighAccuracy: true });
        };


        var onMapWatchSuccess = function (position) {

            var updatedLatitude = position.coords.latitude;
            var updatedLongitude = position.coords.longitude;

            if (updatedLatitude != $scope.posicaoAtual.latitude || updatedLongitude != $scope.posicaoAtual.longitude) {

                $scope.posicaoAtual.latitude = updatedLatitude;
                $scope.posicaoAtual.longitude = updatedLongitude;

                $scope.mapa.markers[0].setPosition(position);
            }
        }

        // Error callback
        function onMapError(error) {
            console.log('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
            var message;
            switch (error.code) {
                case error.PERMISSION_DENIED: message = ("Usuário não compartilhou sua geolocalização");
                    break;
                case error.POSITION_UNAVAILABLE: message = ("Não pode encontrar a localização atual");
                    break;
                case error.TIMEOUT: message = ("Excedeu tempo limete de busca");
                    break;
                default: message = ("Erro desconhecido");
                    break;
            }
            $ionicPopup.alert({
                title: "Falha ao posicionar",
                template: message
            });
        }

        /**
         * A primeira tentativa do plugin é utilizar o navegador do browser, o que no android acaba não funcionando.
         * Por isso foi utilizado essa estratégia para tentar conseguir a localização sem obrigar 
         * o usuário a interagir mais uma vez com o aplicativo
         */
        function onMapErrorFirstTime(error) {
            console.log('code: ' + error.code + '\n' +
                'message: ' + error.message + '\n');
            navigator.geolocation.getCurrentPosition(onMostrarMapaSuccess, onMapError, { maximumAge: 3000, timeout: 20000, enableHighAccuracy: true });
        }
    }]);