angular.module("furbMobile")
    .controller("resolverPerguntaCtrl", ["$scope", "tarefaAPI", "$ionicLoading", "$ionicPopup", function ($scope, tarefaAPI, $ionicLoading, $ionicPopup) {

        $scope.resolverPergunta = function (resposta) {
            $ionicLoading.show({
                template: '<p>Confirmando resposta...</p><ion-spinner></ion-spinner>'
            });
            tarefaAPI
                .resolverPergunta($scope.tarefaGrupo.id, resposta)
                .then(function onSuccess(params) {
                    $ionicLoading.hide();
                    $scope.fecharModal();
                })
                .catch(function onError(params) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: "Não foi possível executar a operação.",
                        template: "Ocorreu um erro ao tentar executar esta operação"
                    });
                });
        };

    }]);