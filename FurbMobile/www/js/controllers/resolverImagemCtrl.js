angular.module("furbMobile")
    .controller("resolverImagemCtrl", ["$scope", "tarefaAPI", "$cordovaCamera", "$ionicLoading", "$ionicPopup", function ($scope, tarefaAPI, $cordovaCamera, $ionicLoading, $ionicPopup) {

        $scope.tirarFoto = function () {
            var options = {
                quality: 30,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                encodingType: Camera.EncodingType.JPEG
            }

            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.imageData = imageData;
                $scope.srcImagem = "data:image/jpeg;base64," + imageData;
            }, function (err) {
                console.log("ERR", err);
            });

        }

        $scope.resolverImagem = function () {
            $ionicLoading.show({
                template: '<p>Confirmando resposta...</p><ion-spinner></ion-spinner>'
            });
            tarefaAPI
                .resolverImagem($scope.tarefaGrupo.id, $scope.imageData)
                .then(function onSuccess(params) {
                    $ionicLoading.hide();
                    $scope.fecharModal();
                })
                .catch(function onError(params) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: "Não foi possível executar a operação.",
                        template: "Ocorreu um erro ao tentar executar esta operação"
                    });
                });
        }

    }]);