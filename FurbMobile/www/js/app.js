angular.module('furbMobile', ['ionic', 'ionic.service.core', 'ionic.service.push', 'ngCordova', 'ngOpenFB', 'ngMap'])

    .run(function ($ionicPlatform, ngFB, $ionicPush, $ionicPopup) {
        ngFB.init({
            appId: '252117715125830',
            status: true,
            cookie: true,
            xfbml: true,
            accessToken: localStorage.getItem('fbAccessToken')
        });

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            var push = new Ionic.Push({
                "debug": true,
                "onNotification": function (notification) {

                    console.log(notification);
                    $ionicPopup.alert({
                        title: notification.title,
                        template: notification.text
                    });

                },
                "onRegister": function (data) {
                    console.log(data.token);
                }
            });
            push.register(function (token) {
                console.log("Device token:", token.token);
                push.saveToken(token);  // persist the token in the Ionic Platform
            });
            
        });
    });