(function () {
    'use strict';
    angular
        .module("furbMobile")
        .value("config", {
            // baseUrl: "http://localhost:8081/jbosswildfly/api/",
            baseUrl: "https://jbosswildfly-furbmobile2016.rhcloud.com/api/",

            // imgUrl: "http://localhost:8081/img/imagens/"
            imgUrl: "https://jbosswildfly-furbmobile2016.rhcloud.com/img/imagens/"
        });
})();