(function () {
    'use strict';
    angular
        .module("furbMobile")
        .filter("situacaoTarefa", function () {
            return function (input) {
                if (input == "0")
                    return "Pendente";
                if (input == "1")
                    return "Assumida";
                if (input == "2")
                    return "Em Análise";
                if (input == "3")
                    return "Finalizada";
            };
        });
})();