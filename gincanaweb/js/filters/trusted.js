(function () {
    'use strict';
    angular
        .module("furbMobile")
        .filter("trusted", ["$sce", function ($sce) {
            return function (url) {
                return $sce.trustAsResourceUrl(url);
            };
        }]);
})();