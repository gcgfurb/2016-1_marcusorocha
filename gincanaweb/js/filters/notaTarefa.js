(function () {
    'use strict';
    angular
        .module("furbMobile")
        .filter("notaTarefa", function () {
            return function (input, situacao) {
                /**
                 * Retorna a nota apenas se a tarefa já foi finalizada (3 - Finalizada)
                 */
                if (situacao == 3) {
                    return input;
                }
                return "N/D";
            };
        });
})();