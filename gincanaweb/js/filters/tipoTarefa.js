(function () {
    'use strict';
    angular
        .module("furbMobile")
        .filter("tipoTarefa", function () {
            return function (input) {
                if (input == "0")
                    return "Pergunta";
                if (input == "1")
                    return "Imagem";
                if (input == "2")
                    return "Video";
                if (input == "3")
                    return "Geolocalização";
            };
        });
})();