(function () {
    'use strict';
	angular
		.module("furbMobile")
		.factory("grupoAPI", ["$http", "config", function ($http, config) {
			var _getGrupos = function (idGincana) {
				return $http.get(config.baseUrl + "grupo/listarporgincana/" + idGincana);
			};

			var _getGrupo = function (id) {
				return $http.get(config.baseUrl + "grupo/" + id);
			};

			var _saveGrupo = function (grupo) {
				return $http.post(config.baseUrl + "grupo/cadastrar", grupo);
			};

			return {
				getGrupos: _getGrupos,
				getGrupo: _getGrupo,
				saveGrupo: _saveGrupo
			};
		}]);
})();