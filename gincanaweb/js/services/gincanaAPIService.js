(function () {
    'use strict';
	angular
		.module("furbMobile")
		.factory("gincanaAPI", ["$http", "config", function ($http, config) {
			var _getGincanas = function () {
				return $http.get(config.baseUrl + "gincana/listar");
			};

			var _getGincana = function (id) {
				return $http.get(config.baseUrl + "gincana/" + id);
			};

			var _saveGincana = function (gincana) {
				return $http.post(config.baseUrl + "gincana/cadastrar", gincana);
			};

			var _ativar = function (id, trello_token) {
				var gincana = { id: id, trello_token: trello_token };
				return $http.post(config.baseUrl + "gincana/ativar/", gincana);
			};

			var _monitorar = function (id) {
				return $http.get(config.baseUrl + "gincana/monitorar/" + id);
			};

			var _finalizar = function (id) {
				return $http.get(config.baseUrl + "gincana/finalizar/" + id);
			};

			return {
				getGincanas: _getGincanas,
				getGincana: _getGincana,
				saveGincana: _saveGincana,
				ativar: _ativar,
				monitorar: _monitorar,
				finalizar: _finalizar
			};
		}]);
})();