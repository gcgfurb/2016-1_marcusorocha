(function () {
    'use strict';
	angular
		.module("furbMobile")
		.factory("tarefaAPI", ["$http", "config", function ($http, config) {
			var _getTarefas = function (idGincana) {
				return $http.get(config.baseUrl + "tarefa/listar/" + idGincana);
			};

			var _getTarefa = function (id) {
				return $http.get(config.baseUrl + "tarefa/" + id);
			};

			var _saveTarefa = function (tarefa) {
				return $http.post(config.baseUrl + "tarefa/cadastrar", tarefa);
			};

			var _vincular = function (idTarefaDependente, listaRequisitos) {
				var dependenciaTarefa = { idTarefaDependente: idTarefaDependente, listaIdTarefaRequisitos: listaRequisitos };
				return $http.post(config.baseUrl + "tarefa/vincular", dependenciaTarefa);
			};

			var _getTarefasDependentes = function (idTarefaDependente) {
				return $http.get(config.baseUrl + "tarefa/listardependentes/" + idTarefaDependente);
			};

			var _avaliarResposta = function (idTarefaGrupo, nota) {
				var tarefaGrupo = { id: idTarefaGrupo, nota: nota };
				return $http.post(config.baseUrl + "tarefa/avaliar", tarefaGrupo);
			};

			return {
				getTarefas: _getTarefas,
				getTarefa: _getTarefa,
				saveTarefa: _saveTarefa,
				vincular: _vincular,
				getTarefasDependentes: _getTarefasDependentes,
				avaliarResposta: _avaliarResposta
			};
		}]);
})();