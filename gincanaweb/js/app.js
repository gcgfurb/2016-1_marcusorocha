(function () {
    'use strict';
    angular
        .module("furbMobile", ["ngRoute", "ui.bootstrap", "ngMap", "ngSanitize"]);
})();