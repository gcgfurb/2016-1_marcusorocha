(function () {
    'use strict';

	angular.module("furbMobile").config(["$routeProvider", function ($routeProvider) {
		$routeProvider.when("/gincanas", {
			templateUrl: "view/gincanas.html",
			controller: "gincanaCtrl"
		});
		$routeProvider.when("/novoGincana", {
			templateUrl: "view/novoGincana.html",
			controller: "novoGincanaCtrl",
			resolve: {
				gincana: function () {
					return undefined;
				}
			}
		});
		$routeProvider.when("/editarGincana/:id", {
			templateUrl: "view/novoGincana.html",
			controller: "novoGincanaCtrl",
			resolve: {
				gincana: function (gincanaAPI, $route) {
					return gincanaAPI.getGincana($route.current.params.id);
				}
			}
		});
		$routeProvider.when("/tarefasGincana/:id", {
			templateUrl: "view/tarefasGincana.html",
			controller: "tarefasGincanaCtrl",
			resolve: {
				gincana: function (gincanaAPI, $route) {
					return gincanaAPI.getGincana($route.current.params.id);
				}
			}
		});
		$routeProvider.when("/novoTarefa/:id", {
			templateUrl: "view/novoTarefa.html",
			controller: "novoTarefaCtrl",
			resolve: {
				idGincana: function ($route) {
					return $route.current.params.id;
				},
				tarefa: function () {
					return undefined;
				}
			}
		});
		$routeProvider.when("/editarTarefa/:id", {
			templateUrl: "view/novoTarefa.html",
			controller: "novoTarefaCtrl",
			resolve: {
				idGincana: function () {
					return undefined;
				},
				tarefa: function (tarefaAPI, $route) {
					return tarefaAPI.getTarefa($route.current.params.id);
				}
			}
		});
		$routeProvider.when("/dependenciaTarefa/:id", {
			templateUrl: "view/dependenciaTarefa.html",
			controller: "dependenciaTarefaCtrl",
			resolve: {
				tarefa: function (tarefaAPI, $route) {
					return tarefaAPI.getTarefa($route.current.params.id);
				}
			}
		});
		$routeProvider.when("/gruposGincana/:id", {
			templateUrl: "view/gruposGincana.html",
			controller: "gruposGincanaCtrl",
			resolve: {
				gincana: function (gincanaAPI, $route) {
					return gincanaAPI.getGincana($route.current.params.id);
				}
			}
		});
		$routeProvider.when("/novoGrupo/:id", {
			templateUrl: "view/novoGrupo.html",
			controller: "novoGrupoCtrl",
			resolve: {
				idGincana: function ($route) {
					return $route.current.params.id;
				},
				grupo: function () {
					return undefined;
				}
			}
		});
		$routeProvider.when("/editarGrupo/:id", {
			templateUrl: "view/novoGrupo.html",
			controller: "novoGrupoCtrl",
			resolve: {
				idGincana: function () {
					return undefined;
				},
				grupo: function (grupoAPI, $route) {
					return grupoAPI.getGrupo($route.current.params.id);
				}
			}
		});
		$routeProvider.when("/ativarGincana/:id", {
			templateUrl: "view/ativarGincana.html",
			controller: "ativarGincanaCtrl",
			resolve: {
				gincana: function (gincanaAPI, $route) {
					return gincanaAPI.getGincana($route.current.params.id);
				}
			}
		});
		$routeProvider.when("/monitorarGincana/:id", {
			templateUrl: "view/monitorarGincana.html",
			controller: "monitorarGincanaCtrl",
			resolve: {
				gincana: function (gincanaAPI, $route) {
					return gincanaAPI.getGincana($route.current.params.id);
				}
			}
		});

		$routeProvider.otherwise({ redirectTo: "/gincanas" });
	}]);

})();