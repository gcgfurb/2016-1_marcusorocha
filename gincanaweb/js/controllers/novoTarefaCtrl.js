(function () {
    'use strict';
    angular
        .module("furbMobile")
        .controller("novoTarefaCtrl", function ($scope, tarefaAPI, $location, idGincana, NgMap, tarefa) {

            $scope.tiposTarefa = [
                { ID: 0, tipo: "Pergunta" },
                { ID: 1, tipo: "Imagem" },
                { ID: 2, tipo: "Video" },
                { ID: 3, tipo: "Geolocalização" }
            ];

            if (tarefa === undefined) {
                $scope.tarefa = { idgincana: idGincana };
            } else {
                $scope.tarefa = tarefa.data;
                if ($scope.tarefa.tipoTarefa == 3) {
                    $scope.posicao = "[" + $scope.tarefa.latitude + "," + $scope.tarefa.longitude + "]";
                    $scope.raio = $scope.tarefa.raio;  
                }
                $scope.tarefa.tipoTarefa = $scope.tiposTarefa[$scope.tarefa.tipoTarefa];                
            }

            $scope.salvarTarefa = function (tarefa) {

                tarefa.tipoTarefa = tarefa.tipoTarefa.ID;

                if (tarefa.tipoTarefa == 3) {
                    NgMap.getMap().then(function (map) {
                        tarefa.latitude = map.shapes.circle.center.lat();
                        tarefa.longitude = map.shapes.circle.center.lng();
                        tarefa.raio = map.shapes.circle.radius;
                        tarefaAPI.saveTarefa(tarefa).success(function (data) {
                            delete $scope.tarefa;
                            $scope.tarefaForm.$setPristine();
                            $location.path("/tarefasGincana/" + tarefa.idgincana);
                        });
                    });
                } else {
                    tarefaAPI.saveTarefa(tarefa).success(function (data) {
                        delete $scope.tarefa;
                        $scope.tarefaForm.$setPristine();
                        $location.path("/tarefasGincana/" + tarefa.idgincana);
                    });
                }
            };

            $scope.voltar = function () {
                $location.path("/tarefasGincana/" + (idGincana === undefined ? $scope.tarefa.idgincana : idGincana));
            };
        });
})();