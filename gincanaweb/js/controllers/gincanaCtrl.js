(function () {
    'use strict';
	angular
		.module("furbMobile")
		.controller("gincanaCtrl", ["$scope", "gincanaAPI", function ($scope, gincanaAPI) {
			gincanaAPI.getGincanas().then(
				function onSuccess(params) {
					$scope.gincanas = params.data;
				}
			);

			$scope.ativarGincana = function (gincana, indice) {

				var opts = {
					type: "popup",
					name: "Furb Mobile - Gincanas",
					scope: {
						read: true,
						write: true,
						account: true
					},
					expiration: "never",
					success: function () {

						gincanaAPI.ativar(gincana.id, localStorage.getItem("trello_token")).then(
							function onSuccess(params) {
								$scope.gincanas[indice] = params.data;
							}
						);
					},
					error: function (params) {
						alert("Não foi possível víncular com o Trelo: " + params);
						console.log(params);
					}
				};
				Trello.authorize(opts);
			};
		}]);
})();