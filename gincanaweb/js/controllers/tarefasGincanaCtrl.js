(function () {
    'use strict';
    angular
        .module("furbMobile")
        .controller("tarefasGincanaCtrl", ["$scope", "tarefaAPI", "gincana", function ($scope, tarefaAPI, gincana) {
            $scope.gincana = gincana.data;
            tarefaAPI.getTarefas(gincana.data.id).then(
                function onSuccess(params) {
                    $scope.tarefas = params.data;
                }
            );
        }]);
})();