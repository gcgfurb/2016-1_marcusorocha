(function () {
    'use strict';
    angular
        .module("furbMobile")
        .controller("gruposGincanaCtrl", ["$scope", "grupoAPI", "gincana", function ($scope, grupoAPI, gincana) {
            $scope.gincana = gincana.data;
            grupoAPI.getGrupos(gincana.data.id).then(
                function onSuccess(params) {
                    $scope.grupos = params.data;
                }
            );
        }]);
})();