(function () {
    'use strict';
    angular
        .module("furbMobile")
        .controller("avaliarRespostaCtrl", ["$scope", "$uibModalInstance", "tarefaAPI", "tarefaGrupo", "config", function ($scope, $uibModalInstance, tarefaAPI, tarefaGrupo, config) {

            $scope.rate = 0;
            $scope.obj = { mostrarMapa: false };

            if (tarefaGrupo.tarefa.tipoTarefa == 1)
                tarefaGrupo.srcImagem = config.imgUrl + tarefaGrupo.resposta;
            else
                if (tarefaGrupo.tarefa.tipoTarefa == 2)
                    tarefaGrupo.srcVideo = "https://www.youtube.com/embed/" + tarefaGrupo.resposta;

            $scope.tarefaGrupo = tarefaGrupo;

            $scope.ok = function () {
                tarefaAPI.avaliarResposta($scope.tarefaGrupo.id, $scope.rate);
                $uibModalInstance.close($scope.tarefaGrupo);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.hoveringOver = function (value) {
                $scope.overStar = value;
                $scope.nota = value;
            };
        }]);
})();