(function () {
    'use strict';
    angular
        .module("furbMobile")
        .controller("novoGrupoCtrl", function ($scope, grupoAPI, $location, idGincana, grupo) {
            $scope.grupo = grupo === undefined ? { idGincana: idGincana } : grupo.data;

            $scope.salvarGrupo = function (grupo) {
                grupoAPI.saveGrupo(grupo).success(function (data) {
                    delete $scope.grupo;
                    $scope.grupoForm.$setPristine();
                    $location.path("/gruposGincana/" + grupo.idGincana);
                });
            };
            $scope.voltar = function () {
                $location.path("/gruposGincana/" + (idGincana === undefined ? $scope.grupo.idGincana : idGincana));
            };
        });
})();