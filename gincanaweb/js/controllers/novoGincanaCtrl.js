(function () {
    'use strict';
	angular
		.module("furbMobile")
		.controller("novoGincanaCtrl", function ($scope, gincanaAPI, $location, gincana) {
			$scope.gincana = gincana === undefined ? {} : gincana.data;

			$scope.salvarGincana = function (gincana) {
				gincanaAPI.saveGincana(gincana).success(function (data) {
					delete $scope.gincana;
					$scope.gincanaForm.$setPristine();
					$location.path("/gincanas");
				});
			};
		});
})();