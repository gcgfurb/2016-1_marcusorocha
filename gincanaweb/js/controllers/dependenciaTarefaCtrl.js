(function () {
    'use strict';
    angular
        .module("furbMobile")
        .controller("dependenciaTarefaCtrl", ["$scope", "$location", "tarefaAPI", "tarefa", function ($scope, $location, tarefaAPI, tarefa) {

            $scope.tarefaP = tarefa.data;
            tarefaAPI.getTarefas($scope.tarefaP.idgincana).then(
                function onSuccess(params) {
                    var _tarefas = params.data;
                    for (var i = 0; i < _tarefas.length; i++)
                        if (_tarefas[i].id == $scope.tarefaP.id) {
                            _tarefas.splice(i, 1);
                            break;
                        }

                    tarefaAPI.getTarefasDependentes($scope.tarefaP.id).then(
                        function onSuccess(params) {
                            var _listaTarefasDTORequisitos = params.data.listaTarefasDTORequisitos;
                            for (var i = 0; i < _listaTarefasDTORequisitos.length; i++) {
                                for (var j = 0; j < _tarefas.length; j++) {
                                    if (_tarefas[j].id == _listaTarefasDTORequisitos[i].id) {
                                        _tarefas[j].selecionado = true;
                                        break;
                                    }
                                }
                            }
                            $scope.tarefas = _tarefas;
                        }
                    );
                }
            );
            $scope.vincularTarefas = function (tarefas) {
                var listaRequisitos = [];
                for (var i = 0; i < tarefas.length; i++) {
                    var t = tarefas[i];
                    if (t.selecionado) {
                        listaRequisitos.push(t.id);
                    }
                }

                tarefaAPI.vincular(tarefa.data.id, listaRequisitos);
                $location.path("/tarefasGincana/" + tarefa.data.idgincana);
            };
            $scope.voltar = function () {
                $location.path("/tarefasGincana/" + tarefa.data.idgincana);
            };
        }]);


})();