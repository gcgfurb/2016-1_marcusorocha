(function () {
    'use strict';
    angular
        .module("furbMobile")
        .controller("monitorarGincanaCtrl", ["$scope", "$uibModal", "gincanaAPI", "gincana", "$location", function ($scope, $uibModal, gincanaAPI, gincana, $location) {

            $scope.gincana = gincana.data;

            gincanaAPI.monitorar($scope.gincana.id).then(
                function onSuccess(params) {
                    $scope.tarefasGincana = params.data;
                }
            );

            $scope.avaliarTarefa = function (tarefaGrupo) {
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'view/avaliarResposta.html',
                    controller: 'avaliarRespostaCtrl',
                    resolve: {
                        tarefaGrupo: function () {
                            return tarefaGrupo;
                        }
                    }
                });

                modalInstance.result.then(function (tarefaGrupo) {
                    for (var i = 0; i < $scope.tarefasGincana.length; i++) {
                        if ($scope.tarefasGincana[i].id == tarefaGrupo.id) {
                            $scope.tarefasGincana[i] = tarefaGrupo;
                            break;
                        }
                    }
                });
            };

            $scope.finalizarGincana = function () {
                gincanaAPI.finalizar($scope.gincana.id).then(
                    function onSuccess(params) {
                        alert("Gincana finalizada com sucesso!\n" + params);
                        $location.path("/gincanas");
                    }
                );
            };

        }]);
})();