angular.module('furbmobile', ['ionic', 'furbmobile.LoginCtrl', 'furbmobile.HomeCtrl', 'ngOpenFB', 'ionic.service.core', 'ionic.service.push'])

    .run(function($ionicPlatform, ngFB) {
        ngFB.init({ appId: '163060027414387', 
                    status: true, 
                    cookie: true,
                    xfbml: true,
                    accessToken: localStorage.getItem('tokenfacebook')
                 });

        $ionicPlatform.ready(function() {
			
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state('login', {
                url: '/login',
                // templateUrl: '../pages/login/login.html', // RODAR NO BROWSER
                templateUrl: '../www/pages/login/login.html',// RODAR NO CELULAR
                controller: 'LoginCtrl'
            })

            .state('home-page', {
                url: '/home',
                 //templateUrl: '../pages/home/home.html', // RODAR NO BROWSER
                templateUrl: '../www/pages/home/home.html',// RODAR NO CELULAR
                controller: 'HomeCtrl'
            })

        $urlRouterProvider.otherwise('/login');
    });
