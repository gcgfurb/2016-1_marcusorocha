angular.module('furbmobile.LoginCtrl', [])

    .controller('LoginCtrl', function($scope, $ionicPopup, $state, $timeout, ngFB) {
        $scope.data = {};

        $scope.login = function() {

            ngFB.getLoginStatus().then(
                function(response) {
                    if (response.status === 'connected') {
                        // the user is logged in and has authenticated your
                        // app, and response.authResponse supplies
                        // the user's ID, a valid access token, a signed
                        // request, and the time the access token 
                        // and signed request each expire
                        //var uid = response.authResponse.userID;
                        //var accessToken = response.authResponse.accessToken;
                        $state.go('home-page');
                    } else if (response.status === 'not_authorized') {
                        // the user is logged in to Facebook, 
                        // but has not authenticated your app
                        alert('App não autorizado');
                    } else {
                        // the user isn't logged in to Facebook.
                        ngFB.login({ scope: 'public_profile,user_location' }).then(
                            function(response) {
                                if (response.status === 'connected') {
                                    console.log('Facebook login succeeded');
                                    localStorage.setItem('tokenfacebook', response.authResponse.accessToken)
                                    $state.go('home-page');
                                } else {
                                    alert('Facebook login failed');
                                }
                            });
                    }
                });


        };

    });