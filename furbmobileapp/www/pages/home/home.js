angular.module('furbmobile.HomeCtrl', [])

    .controller('HomeCtrl', function($scope, $state, ngFB, $ionicPush, $ionicPopup) {

        $ionicPush.init({
            "debug": true,
            "onNotification": function(notification) {
                var payload = notification.payload;
                console.log(notification, payload);
                
                $ionicPopup.alert({
                    title: 'Notificação',
                    template: payload
                }).then();
            },
            "onRegister": function(data) {
                console.log(data.token);
            }
        });

        $ionicPush.register();


        ngFB.api({
            path: '/me',
            params: { fields: 'id,name,first_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified,location' }
        }).then(
            function(user) {
                $scope.user = user;
            },
            function(error) {
                alert('Facebook error: ' + error.error_description);
            });

    });